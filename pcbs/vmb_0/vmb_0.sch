EESchema Schematic File Version 4
LIBS:vmb_0-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "VMB"
Date "2019-10-29"
Rev ""
Comp "LHDT"
Comment1 "VMB beta 0.0"
Comment2 "Dennis Mitten"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+5V #PWR0101
U 1 1 5DB8CBDC
P 4800 4350
F 0 "#PWR0101" H 4800 4200 50  0001 C CNN
F 1 "+5V" H 4815 4523 50  0000 C CNN
F 2 "" H 4800 4350 50  0001 C CNN
F 3 "" H 4800 4350 50  0001 C CNN
	1    4800 4350
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0102
U 1 1 5DB8D607
P 700 5450
F 0 "#PWR0102" H 700 5200 50  0001 C CNN
F 1 "Earth" H 700 5300 50  0001 C CNN
F 2 "" H 700 5450 50  0001 C CNN
F 3 "~" H 700 5450 50  0001 C CNN
	1    700  5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	700  4500 700  4300
Wire Wire Line
	700  4700 700  4500
Connection ~ 700  4500
Wire Wire Line
	700  5200 700  5450
$Comp
L Transistor_BJT:BC547 Q0
U 1 1 5DB977B6
P 1850 3300
F 0 "Q0" V 2085 3300 50  0000 C CNN
F 1 "BC547" V 2176 3300 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 2050 3225 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC547.pdf" H 1850 3300 50  0001 L CNN
	1    1850 3300
	0    1    1    0   
$EndComp
$Comp
L Transistor_BJT:BC547 Q1
U 1 1 5DBA04A5
P 2100 2950
F 0 "Q1" V 2335 2950 50  0000 C CNN
F 1 "BC547" V 2426 2950 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 2300 2875 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC547.pdf" H 2100 2950 50  0001 L CNN
	1    2100 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	1250 4400 1200 4400
$Comp
L Transistor_BJT:BC547 Q2
U 1 1 5DBB40A2
P 2400 2600
F 0 "Q2" V 2635 2600 50  0000 C CNN
F 1 "BC547" V 2726 2600 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 2600 2525 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC547.pdf" H 2400 2600 50  0001 L CNN
	1    2400 2600
	0    1    1    0   
$EndComp
Wire Wire Line
	1650 3400 1200 3400
Wire Wire Line
	700  5200 700  5100
Connection ~ 700  4700
Connection ~ 700  5100
Wire Wire Line
	700  5100 700  4700
NoConn ~ 1850 4400
NoConn ~ 1850 5000
NoConn ~ 1850 5200
NoConn ~ 1250 5000
NoConn ~ 1250 4900
Wire Wire Line
	1850 3100 1850 2750
Wire Wire Line
	2100 2750 2100 2400
Wire Wire Line
	2100 2400 2400 2400
Wire Wire Line
	1850 2750 2100 2750
Connection ~ 2100 2750
Connection ~ 700  5200
$Comp
L Connector:DB15_Female_HighDensity J0
U 1 1 5DB8A198
P 1550 4800
F 0 "J0" H 1650 5500 50  0000 C CNN
F 1 "DB15_Female_HighDensity" H 1550 5576 50  0001 C CNN
F 2 "" H 600 5200 50  0001 C CNN
F 3 " ~" H 600 5200 50  0001 C CNN
	1    1550 4800
	1    0    0    -1  
$EndComp
Text Label 2950 2950 2    50   ~ 0
Bin
Text Label 2950 3150 2    50   ~ 0
Rin
Text Label 2950 3050 2    50   ~ 0
Gin
Wire Wire Line
	2600 2700 2600 2950
Wire Wire Line
	2600 3150 2600 3400
Wire Wire Line
	2050 3400 2600 3400
Text Label 2750 3700 1    50   ~ 0
RGB_OEin
Text Notes 7000 1450 0    50   ~ 0
Video Memory
Wire Notes Line
	2950 3700 2950 2100
Wire Notes Line
	900  2100 900  3700
Text Notes 900  2200 0    50   ~ 0
RGB Enable Part
Text Label 4300 4800 1    50   ~ 0
40MHzClk
Wire Notes Line
	5850 5700 5850 3900
Text Notes 2750 4000 0    50   ~ 0
Clock generation part (UNDONE)
Wire Notes Line
	5800 1850 5800 3700
Wire Notes Line
	5800 3700 3150 3700
Wire Notes Line
	3150 3700 3150 1850
Wire Notes Line
	3150 1850 5800 1850
Text Notes 3150 1950 0    50   ~ 0
RGB decoder part (UNDONE)
Text Label 3150 2950 0    50   ~ 0
Bout
Text Label 3150 3050 0    50   ~ 0
Gout
Text Label 3150 3150 0    50   ~ 0
Rout
Wire Wire Line
	2600 2950 3650 2950
Wire Wire Line
	2300 3050 3650 3050
Wire Wire Line
	2600 3150 3650 3150
Text Notes 5950 600  0    50   ~ 0
Video memory part (UNDONE)
Wire Notes Line
	5950 4000 9150 4000
Wire Notes Line
	5950 500  9150 500 
$Comp
L power:Earth #PWR0104
U 1 1 5DCD02DD
P 7750 1050
F 0 "#PWR0104" H 7750 800 50  0001 C CNN
F 1 "Earth" H 7750 900 50  0001 C CNN
F 2 "" H 7750 1050 50  0001 C CNN
F 3 "~" H 7750 1050 50  0001 C CNN
	1    7750 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 1300 8050 2450
Text Label 2750 5200 0    50   ~ 0
VclkOUT
Text Label 2750 5100 0    50   ~ 0
HclkOUT
Wire Notes Line
	2750 3900 2750 5700
Wire Wire Line
	2650 5200 3200 5200
Wire Wire Line
	2700 5100 3200 5100
Wire Wire Line
	1850 4800 2650 4800
Wire Wire Line
	2650 4800 2650 5200
Wire Wire Line
	1850 4600 2700 4600
Wire Wire Line
	2700 4600 2700 5100
Wire Wire Line
	1000 2700 1000 4800
Wire Wire Line
	1100 3050 1100 4600
Wire Wire Line
	1200 3400 1200 4400
Text Label 1200 3700 1    50   ~ 0
Rout
Wire Wire Line
	1000 2700 2200 2700
Wire Wire Line
	1100 3050 1900 3050
Wire Wire Line
	1100 4600 1250 4600
Wire Wire Line
	1000 4800 1250 4800
Text Label 1100 3700 1    50   ~ 0
Gout
Text Label 1000 3700 1    50   ~ 0
Bout
Wire Wire Line
	700  4300 1250 4300
Wire Wire Line
	700  4500 1250 4500
Wire Wire Line
	700  4700 1250 4700
Wire Wire Line
	700  5100 1250 5100
Wire Wire Line
	700  5200 1250 5200
Wire Notes Line
	2600 3900 2600 5700
Wire Notes Line
	2600 5700 500  5700
Wire Notes Line
	500  5700 500  3900
Wire Notes Line
	500  3900 2600 3900
Text Notes 800  4000 2    50   ~ 0
VGA out
Text Label 1200 3900 3    50   ~ 0
Rin
Text Label 1100 3900 3    50   ~ 0
Gin
Text Label 1000 3900 3    50   ~ 0
Bin
Wire Notes Line
	900  2100 2950 2100
Wire Notes Line
	900  3700 2950 3700
Wire Wire Line
	2750 2400 2400 2400
Connection ~ 2400 2400
Wire Wire Line
	2750 3850 4650 3850
Wire Wire Line
	2750 2400 2750 3850
Text Label 4650 3900 3    50   ~ 0
RGB_OEout
Text Notes 4100 2400 0    50   ~ 0
Something clever here
Wire Notes Line
	4000 2150 5150 2150
Wire Notes Line
	5150 2150 5150 3250
Wire Notes Line
	5150 3250 4000 3250
Wire Notes Line
	4000 3250 4000 2150
$Comp
L rma_components:CY7C1049DV33-10VXI IC0
U 1 1 5DB8D94E
P 6650 1550
F 0 "IC0" V 7300 700 50  0000 C CNN
F 1 "CY7C1049DV33-10VXI" V 7200 700 50  0000 C CNN
F 2 "SOJ-36" H 7700 1650 50  0001 L CNN
F 3 "http://uk.rs-online.com/web/p/products/7105714" H 7700 1550 50  0001 L CNN
F 4 "Cypress Semiconductor CY7C1049DV33-10VXI SRAM Memory, 4Mbit, 3  3.6 V, 10ns 36-Pin SOJ" H 7700 1450 50  0001 L CNN "Description"
F 5 "Cypress Semiconductor" H 7700 1250 50  0001 L CNN "Manufacturer_Name"
F 6 "CY7C1049DV33-10VXI" H 7700 1150 50  0001 L CNN "Manufacturer_Part_Number"
F 7 "727-CYC1049DV3310VXI" H 7700 1050 50  0001 L CNN "Mouser Part Number"
F 8 "https://www.mouser.com/Search/Refine.aspx?Keyword=727-CYC1049DV3310VXI" H 7700 950 50  0001 L CNN "Mouser Price/Stock"
F 9 "7105714P" H 7700 850 50  0001 L CNN "RS Part Number"
F 10 "http://uk.rs-online.com/web/p/products/7105714P" H 7700 750 50  0001 L CNN "RS Price/Stock"
	1    6650 1550
	1    0    0    -1  
$EndComp
Text Notes 500  600  0    50   ~ 0
BC547 analog (better than bc547) is 2N3904
$Comp
L Oscillator:ACO-xxxMHz X0
U 1 1 5DBC6F1A
P 4800 4800
F 0 "X0" V 4450 4850 50  0000 R CNN
F 1 "ACO-40MHz" V 4550 5000 50  0000 R CNN
F 2 "Oscillator:Oscillator_DIP-14" H 5250 4450 50  0001 C CNN
F 3 "http://www.conwin.com/datasheets/cx/cx030.pdf" H 4700 4800 50  0001 C CNN
	1    4800 4800
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R0
U 1 1 5DBB9BF5
P 6250 1250
F 0 "R0" V 6250 1200 50  0000 L CNN
F 1 "R 1K" V 6150 1100 50  0000 L CNN
F 2 "" V 6180 1250 50  0001 C CNN
F 3 "~" H 6250 1250 50  0001 C CNN
	1    6250 1250
	0    1    1    0   
$EndComp
Connection ~ 4650 3850
Text Label 5950 3850 0    50   ~ 0
RGB_OEin
Wire Wire Line
	4650 3850 6350 3850
Wire Wire Line
	6900 3850 8200 3850
Wire Notes Line
	5950 500  5950 4000
Wire Notes Line
	9700 500  9700 4000
$Comp
L power:+5V #PWR0106
U 1 1 5DBF60F2
P 7050 3750
F 0 "#PWR0106" H 7050 3600 50  0001 C CNN
F 1 "+5V" H 7050 3900 50  0000 C CNN
F 2 "" H 7050 3750 50  0001 C CNN
F 3 "" H 7050 3750 50  0001 C CNN
	1    7050 3750
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0107
U 1 1 5DBFA053
P 7200 3650
F 0 "#PWR0107" H 7200 3400 50  0001 C CNN
F 1 "Earth" H 7200 3500 50  0001 C CNN
F 2 "" H 7200 3650 50  0001 C CNN
F 3 "~" H 7200 3650 50  0001 C CNN
	1    7200 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 3650 7100 3650
Wire Wire Line
	7100 3650 7100 3950
$Comp
L 74xGxx:74LVC1G04 U0
U 1 1 5DBCCE63
P 6650 3850
F 0 "U0" H 6600 3850 50  0000 C CNN
F 1 "74LVC1G04" H 6625 4026 50  0000 C CNN
F 2 "" H 6650 3850 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 6650 3850 50  0001 C CNN
	1    6650 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 3950 7100 3950
Wire Wire Line
	6650 3750 7050 3750
Wire Notes Line
	2750 3900 5850 3900
Wire Notes Line
	2750 5700 5850 5700
Text Label 2600 4600 2    50   ~ 0
HclkIN
Text Label 2600 4800 2    50   ~ 0
VclkIN
Wire Wire Line
	5500 4050 6150 4050
Wire Wire Line
	5500 4250 6150 4250
Wire Wire Line
	5500 4300 6150 4300
Wire Wire Line
	5500 4350 6150 4350
Wire Wire Line
	5500 4400 6150 4400
Wire Wire Line
	5500 4450 6150 4450
Wire Wire Line
	5500 4500 6150 4500
Text Label 6150 4050 2    25   ~ 0
X0
Text Label 6150 4150 2    25   ~ 0
X2
Text Label 6150 4100 2    25   ~ 0
X1
Text Label 6150 4200 2    25   ~ 0
X3
Text Label 6150 4250 2    25   ~ 0
X4
Text Label 6150 4300 2    25   ~ 0
X5
Text Label 6150 4350 2    25   ~ 0
X6
Text Label 6150 4400 2    25   ~ 0
X7
Text Label 6150 4450 2    25   ~ 0
X8
Text Label 6150 4500 2    25   ~ 0
X9
Wire Wire Line
	5500 4600 6150 4600
Wire Wire Line
	5500 4650 6150 4650
Wire Wire Line
	5500 4700 6150 4700
Wire Wire Line
	5500 4750 6150 4750
Wire Wire Line
	5500 4800 6150 4800
Wire Wire Line
	5500 4850 6150 4850
Wire Wire Line
	5500 4900 6150 4900
Wire Wire Line
	5500 4950 6150 4950
Wire Wire Line
	5500 5000 6150 5000
Wire Wire Line
	5500 5050 6150 5050
Text Label 6150 4600 2    25   ~ 0
Y0
Text Label 6150 4700 2    25   ~ 0
Y2
Text Label 6150 4650 2    25   ~ 0
Y1
Text Label 6150 4750 2    25   ~ 0
Y3
Text Label 6150 4800 2    25   ~ 0
Y4
Text Label 6150 4850 2    25   ~ 0
Y5
Text Label 6150 4900 2    25   ~ 0
Y6
Text Label 6150 4950 2    25   ~ 0
Y7
Text Label 6150 5000 2    25   ~ 0
Y8
Text Label 6150 5050 2    25   ~ 0
Y9
Entry Wire Line
	6150 4050 6250 4150
Entry Wire Line
	6150 4100 6250 4200
Entry Wire Line
	6150 4150 6250 4250
Entry Wire Line
	6150 4200 6250 4300
Entry Wire Line
	6150 4250 6250 4350
Entry Wire Line
	6150 4300 6250 4400
Entry Wire Line
	6150 4350 6250 4450
Entry Wire Line
	6150 4400 6250 4500
Entry Wire Line
	6150 4500 6250 4600
Entry Wire Line
	6150 4450 6250 4550
Entry Wire Line
	6150 4600 6250 4700
Entry Wire Line
	6150 4650 6250 4750
Entry Wire Line
	6150 4700 6250 4800
Entry Wire Line
	6150 4750 6250 4850
Entry Wire Line
	6150 4800 6250 4900
Entry Wire Line
	6150 4850 6250 4950
Entry Wire Line
	6150 4900 6250 5000
Entry Wire Line
	6150 4950 6250 5050
Entry Wire Line
	6150 5000 6250 5100
Entry Wire Line
	6150 5050 6250 5150
Entry Wire Line
	8300 3500 8400 3400
Entry Wire Line
	8300 3550 8400 3450
Entry Wire Line
	8300 3600 8400 3500
Entry Wire Line
	8300 3650 8400 3550
Entry Wire Line
	8300 3700 8400 3600
Entry Wire Line
	8300 3750 8400 3650
Entry Wire Line
	8300 3800 8400 3700
Entry Wire Line
	8300 3850 8400 3750
Entry Wire Line
	8300 3950 8400 3850
Entry Wire Line
	8300 3900 8400 3800
Wire Bus Line
	6250 4600 8300 4600
Text Label 6300 4600 0    50   ~ 0
Xbus
Wire Bus Line
	6250 5150 8950 5150
Text Label 6300 5150 0    50   ~ 0
Ybus
Wire Wire Line
	7850 2050 8200 2050
Wire Wire Line
	8200 2050 8200 3850
Entry Wire Line
	8100 2250 8000 2150
Entry Wire Line
	8100 2350 8000 2250
Wire Wire Line
	7850 2450 8050 2450
Wire Wire Line
	7850 2350 7900 2350
Entry Wire Line
	8100 2650 8000 2550
Entry Wire Line
	8100 2750 8000 2650
Entry Wire Line
	6500 2550 6400 2650
Entry Wire Line
	6500 2650 6400 2750
Entry Wire Line
	6400 2350 6500 2250
Wire Wire Line
	6650 2350 6600 2350
Wire Wire Line
	6650 2750 6600 2750
Entry Wire Line
	6400 2250 6500 2150
Text Label 5950 2500 0    25   ~ 0
databus_out
Text Label 5800 2500 2    25   ~ 0
databus_in
NoConn ~ 5350 2500
Wire Wire Line
	6100 2400 6100 2050
Wire Wire Line
	8400 3400 8400 3250
Wire Wire Line
	8400 3450 8450 3450
Wire Wire Line
	8450 3450 8450 3250
Wire Wire Line
	8400 3500 8500 3500
Wire Wire Line
	8500 3500 8500 3250
Wire Wire Line
	8400 3550 8550 3550
Wire Wire Line
	8550 3550 8550 3250
Wire Wire Line
	8400 3600 8600 3600
Wire Wire Line
	8600 3600 8600 3250
Wire Wire Line
	8400 3650 8650 3650
Wire Wire Line
	8650 3650 8650 3250
Wire Wire Line
	8400 3700 8700 3700
Wire Wire Line
	8700 3700 8700 3250
Wire Wire Line
	8400 3750 8750 3750
Wire Wire Line
	8750 3750 8750 3250
Wire Wire Line
	8400 3800 8800 3800
Wire Wire Line
	8800 3800 8800 3250
Wire Wire Line
	8400 3850 8850 3850
Wire Wire Line
	8850 3850 8850 3250
Text Label 8400 3400 1    25   ~ 0
X0
Text Label 8400 3450 0    25   ~ 0
X1
Text Label 8400 3500 0    25   ~ 0
X2
Text Label 8400 3550 0    25   ~ 0
X3
Text Label 8400 3600 0    25   ~ 0
X4
Text Label 8400 3650 0    25   ~ 0
X5
Text Label 8400 3700 0    25   ~ 0
X6
Text Label 8400 3750 0    25   ~ 0
X7
Text Label 8400 3800 0    25   ~ 0
X8
Text Label 8400 3850 0    25   ~ 0
X9
Entry Wire Line
	8950 3500 9050 3400
Entry Wire Line
	8950 3550 9050 3450
Entry Wire Line
	8950 3600 9050 3500
Entry Wire Line
	8950 3650 9050 3550
Entry Wire Line
	8950 3700 9050 3600
Entry Wire Line
	8950 3750 9050 3650
Entry Wire Line
	8950 3800 9050 3700
Entry Wire Line
	8950 3850 9050 3750
Entry Wire Line
	8950 3950 9050 3850
Entry Wire Line
	8950 3900 9050 3800
Wire Wire Line
	9050 3400 9050 3250
Wire Wire Line
	9050 3450 9100 3450
Wire Wire Line
	9100 3450 9100 3250
Wire Wire Line
	9050 3500 9150 3500
Wire Wire Line
	9150 3500 9150 3250
Wire Wire Line
	9050 3550 9200 3550
Wire Wire Line
	9200 3550 9200 3250
Wire Wire Line
	9050 3600 9250 3600
Wire Wire Line
	9250 3600 9250 3250
Wire Wire Line
	9050 3650 9300 3650
Wire Wire Line
	9300 3650 9300 3250
Wire Wire Line
	9050 3700 9350 3700
Wire Wire Line
	9350 3700 9350 3250
Wire Wire Line
	9050 3750 9400 3750
Wire Wire Line
	9400 3750 9400 3250
Wire Wire Line
	9050 3800 9450 3800
Wire Wire Line
	9450 3800 9450 3250
Wire Wire Line
	9050 3850 9500 3850
Wire Wire Line
	9500 3850 9500 3250
Text Label 9050 3400 1    25   ~ 0
Y0
Text Label 9050 3450 0    25   ~ 0
Y1
Text Label 9050 3500 0    25   ~ 0
Y2
Text Label 9050 3550 0    25   ~ 0
Y3
Text Label 9050 3600 0    25   ~ 0
Y4
Text Label 9050 3650 0    25   ~ 0
Y5
Text Label 9050 3700 0    25   ~ 0
Y6
Text Label 9050 3750 0    25   ~ 0
Y7
Text Label 9050 3800 0    25   ~ 0
Y8
Text Label 9050 3850 0    25   ~ 0
Y9
Text Label 6500 2150 0    25   ~ 0
DB0
Text Label 6500 2250 0    25   ~ 0
DB1
Text Label 6500 2550 0    25   ~ 0
DB2
Text Label 6500 2650 0    25   ~ 0
DB3
Text Label 8000 2650 2    25   ~ 0
DB4
Text Label 8000 2550 2    25   ~ 0
DB5
Text Label 8000 2250 2    25   ~ 0
DB6
Text Label 8000 2150 2    25   ~ 0
DB7
Wire Wire Line
	7900 2350 7900 1350
Wire Wire Line
	7850 2150 8000 2150
Wire Wire Line
	8000 2250 7850 2250
Wire Wire Line
	7850 2550 8000 2550
Wire Wire Line
	7850 2650 8000 2650
Wire Wire Line
	6500 2150 6650 2150
Wire Wire Line
	6650 2250 6500 2250
Wire Wire Line
	6500 2550 6650 2550
Wire Wire Line
	6650 2650 6500 2650
Wire Bus Line
	6400 2500 5350 2500
Connection ~ 6400 2500
Wire Bus Line
	6400 3400 8100 3400
NoConn ~ 5500 4050
NoConn ~ 5500 4200
Wire Wire Line
	5500 4200 6150 4200
Wire Wire Line
	5500 4150 6150 4150
Wire Wire Line
	5500 4100 6150 4100
NoConn ~ 5500 4100
NoConn ~ 5500 4150
NoConn ~ 5500 4250
NoConn ~ 5500 4300
NoConn ~ 5500 4350
NoConn ~ 5500 4400
NoConn ~ 5500 4450
NoConn ~ 5500 4500
NoConn ~ 5500 4600
NoConn ~ 5500 4650
NoConn ~ 5500 4750
NoConn ~ 5500 4800
NoConn ~ 5500 4850
NoConn ~ 5500 4700
NoConn ~ 5500 4900
NoConn ~ 5500 4950
NoConn ~ 5500 5000
NoConn ~ 5500 5050
NoConn ~ 8400 3250
NoConn ~ 8450 3250
NoConn ~ 8550 3250
NoConn ~ 8600 3250
NoConn ~ 8500 3250
NoConn ~ 8650 3250
NoConn ~ 8750 3250
NoConn ~ 8800 3250
NoConn ~ 8850 3250
NoConn ~ 8700 3250
NoConn ~ 9050 3250
NoConn ~ 9100 3250
NoConn ~ 9150 3250
NoConn ~ 9200 3250
NoConn ~ 9250 3250
NoConn ~ 9400 3250
NoConn ~ 9500 3250
NoConn ~ 9450 3250
NoConn ~ 9350 3250
NoConn ~ 9300 3250
Wire Wire Line
	3650 2950 3650 3050
Connection ~ 3650 3050
Wire Wire Line
	3650 3050 3650 3150
$Comp
L power:Earth #PWR0109
U 1 1 5E061B39
P 4650 4400
F 0 "#PWR0109" H 4650 4150 50  0001 C CNN
F 1 "Earth" H 4650 4250 50  0001 C CNN
F 2 "" H 4650 4400 50  0001 C CNN
F 3 "~" H 4650 4400 50  0001 C CNN
	1    4650 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 3850 4650 4400
Wire Wire Line
	6400 1250 6600 1250
Connection ~ 6600 1250
Wire Wire Line
	6600 1250 8050 1250
Wire Wire Line
	7750 1050 7900 1050
Wire Wire Line
	7900 1050 7900 1350
Connection ~ 7900 1350
Wire Wire Line
	4800 5100 4800 5300
Wire Wire Line
	4800 4350 4800 4500
Wire Wire Line
	6600 1150 6600 1250
$Comp
L power:+5V #PWR0105
U 1 1 5DCD448F
P 6600 1150
F 0 "#PWR0105" H 6600 1000 50  0001 C CNN
F 1 "+5V" H 6615 1323 50  0000 C CNN
F 2 "" H 6600 1150 50  0001 C CNN
F 3 "" H 6600 1150 50  0001 C CNN
	1    6600 1150
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0103
U 1 1 5E0FBAB3
P 4800 5300
F 0 "#PWR0103" H 4800 5050 50  0001 C CNN
F 1 "Earth" H 4800 5150 50  0001 C CNN
F 2 "" H 4800 5300 50  0001 C CNN
F 3 "~" H 4800 5300 50  0001 C CNN
	1    4800 5300
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0108
U 1 1 5E0FF331
P 3650 3150
F 0 "#PWR0108" H 3650 2900 50  0001 C CNN
F 1 "Earth" H 3650 3000 50  0001 C CNN
F 2 "" H 3650 3150 50  0001 C CNN
F 3 "~" H 3650 3150 50  0001 C CNN
	1    3650 3150
	1    0    0    -1  
$EndComp
Connection ~ 3650 3150
Wire Notes Line
	8300 800  9600 800 
Wire Notes Line
	9600 800  9600 3300
Wire Notes Line
	9600 3300 8300 3300
Wire Notes Line
	8300 3300 8300 800 
Text Notes 9300 1950 2    50   ~ 0
Addressation block
Entry Wire Line
	8100 1550 8000 1650
Entry Wire Line
	8100 1850 8000 1950
Entry Wire Line
	8100 1650 8000 1750
Entry Wire Line
	8100 1750 8000 1850
Wire Wire Line
	8000 1650 7850 1650
Wire Wire Line
	7850 1750 8000 1750
Wire Wire Line
	8000 1850 7850 1850
Wire Wire Line
	7850 1950 8000 1950
Entry Wire Line
	8000 2850 7900 2750
Entry Wire Line
	8000 2950 7900 2850
Entry Wire Line
	8000 3050 7900 2950
Entry Wire Line
	8000 3150 7900 3050
Entry Wire Line
	8000 3250 7900 3150
Wire Wire Line
	7850 2750 7900 2750
Wire Wire Line
	7900 2850 7850 2850
Wire Wire Line
	7850 2950 7900 2950
Wire Wire Line
	7900 3050 7850 3050
Wire Wire Line
	7850 3150 7900 3150
Text Label 7900 2750 2    25   ~ 0
A14
Text Label 7900 2850 2    25   ~ 0
A13
Text Label 7900 2950 2    25   ~ 0
A12
Text Label 7900 3050 2    25   ~ 0
A11
Text Label 7900 3150 2    25   ~ 0
A10
Wire Bus Line
	8100 1850 8150 1850
Wire Bus Line
	8150 1850 8150 2850
Wire Bus Line
	8150 2850 8000 2850
Connection ~ 8150 1850
Text Label 8300 1850 0    25   ~ 0
MemoryAddress
Entry Wire Line
	6450 2850 6350 2950
Entry Wire Line
	6450 2950 6350 3050
Entry Wire Line
	6450 3050 6350 3150
Entry Wire Line
	6450 3150 6350 3250
Entry Wire Line
	6450 3250 6350 3350
Wire Bus Line
	8150 3450 8150 2850
Connection ~ 8150 2850
Entry Wire Line
	6450 1550 6350 1450
Entry Wire Line
	6450 1650 6350 1550
Entry Wire Line
	6450 1750 6350 1650
Entry Wire Line
	6450 1850 6350 1750
Entry Wire Line
	6450 1950 6350 1850
Wire Wire Line
	6100 2050 6650 2050
Text Label 6450 2850 0    25   ~ 0
A5
Text Label 6450 2950 0    25   ~ 0
A6
Text Label 6450 3050 0    25   ~ 0
A7
Text Label 6450 3150 0    25   ~ 0
A8
Text Label 6450 3250 0    25   ~ 0
A9
Wire Wire Line
	6600 2400 6100 2400
Wire Wire Line
	6600 2750 6600 2400
Wire Bus Line
	6350 3450 8150 3450
Wire Wire Line
	6450 2850 6650 2850
Wire Wire Line
	6450 2950 6650 2950
Wire Wire Line
	6450 3050 6650 3050
Wire Wire Line
	6450 3150 6650 3150
Wire Wire Line
	6450 3250 6650 3250
Text Label 6450 1550 0    25   ~ 0
A0
Text Label 6450 1650 0    25   ~ 0
A1
Text Label 6450 1750 0    25   ~ 0
A2
Text Label 6450 1850 0    25   ~ 0
A3
Text Label 6450 1950 0    25   ~ 0
A4
Wire Wire Line
	6450 1550 6650 1550
Wire Wire Line
	6450 1650 6650 1650
Wire Wire Line
	6450 1750 6650 1750
Wire Wire Line
	6450 1850 6650 1850
Wire Wire Line
	6450 1950 6650 1950
Wire Wire Line
	6300 2450 6300 1350
Wire Wire Line
	6300 2450 6650 2450
Wire Wire Line
	6300 1350 7900 1350
Wire Wire Line
	6100 1250 6100 2050
Connection ~ 6100 2050
Wire Wire Line
	6600 1250 6600 2350
NoConn ~ 7850 1550
Text Label 8000 1650 2    25   ~ 0
A18
Text Label 8000 1750 2    25   ~ 0
A17
Text Label 8000 1850 2    25   ~ 0
A16
Text Label 8000 1950 2    25   ~ 0
A15
NoConn ~ 7850 3250
$Comp
L power:Earth #PWR0110
U 1 1 5E357680
P 3200 5250
F 0 "#PWR0110" H 3200 5000 50  0001 C CNN
F 1 "Earth" H 3200 5100 50  0001 C CNN
F 2 "" H 3200 5250 50  0001 C CNN
F 3 "~" H 3200 5250 50  0001 C CNN
	1    3200 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 5200 3200 5250
Wire Wire Line
	3200 4800 3200 5100
Wire Wire Line
	3200 4800 4500 4800
Wire Bus Line
	8150 1850 8650 1850
NoConn ~ 8650 1850
Wire Bus Line
	6400 2250 6400 2500
Wire Bus Line
	6400 2500 6400 3400
Wire Bus Line
	8100 1550 8100 1850
Wire Bus Line
	8000 2850 8000 3250
Wire Bus Line
	8100 2250 8100 3400
Wire Bus Line
	6350 1450 6350 3450
Wire Bus Line
	8950 3500 8950 5150
Wire Bus Line
	8300 3500 8300 4600
Wire Bus Line
	6250 4700 6250 5150
Wire Bus Line
	6250 4150 6250 4600
$EndSCHEMATC
