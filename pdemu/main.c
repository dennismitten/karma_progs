#define GEN_REG_COUNT 128 // allocated bytes for registers

#define CH_W 5
#define CH_H 16

#define CHPOS_X(x) (2+CH_W*x)
#define CHPOS_Y(y) (2+CH_H*y)

#define SIZE_ERR(s) printf("Unsupported value size: 0x%2x (%d)\n", s, s); exit(5)
#define POP_ERR printf("Stack underflow!\n")

#define EMU_FONT_PREFIX "fonts/"
#define EMU_FONT EMU_FONT_PREFIX "thick-koi8-r.pcf.gz"

// 1 pixel between chars
#define EMU_WIDTH (2+(CH_W+3)*80)
#define EMU_HEIGHT (2+(CH_H+3)*25)

#define EMU_AUDIO_BPS 128

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

// Graphics
#include <SDL/SDL.h>
#include <SDL/SDL_gfxBlitFunc.h>
#include <SDL/SDL_gfxPrimitives.h>
#include <SDL/SDL_ttf.h>

// Audio
#include <alsa/asoundlib.h>

// Threads
#include <pthread.h>

#include "rma_std/rmasm.h"

struct vm_s
{
    // IDB

    uint8_t r0l, r0h;
    uint16_t r0x;
    uint32_t r0f;
    uint32_t r0s;

    uint8_t r1l, r1h;
    uint16_t r1x;
    uint32_t r1f;
    uint32_t r1s;

    uint32_t rip; // instruction pointer (exec ptr)
    uint32_t rbp; // base pointer
    uint32_t rsp; // stack pointer
    //

    // MMB
    size_t   ram_size;
    uint8_t* ram_ptr;
    //

    // VMB - video management block
    uint8_t *vid_buff_ptr; // allocated when init: "int %vmb, 0x76"
    uint16_t rvx;
    uint16_t rvy;
    uint8_t rvc;
    uint8_t rvm;

    uint8_t rv0;
    uint8_t rv1;
    uint8_t rv2;
    uint8_t rv3;
    //

    // AMB - audio management block
    uint8_t *snd_buff_ptr; // sound buffer (int %amb, 0x76)
    uint8_t rav; // audio volume (0-255)
    //
}__attribute__((packed));
typedef struct vm_s* vm_ptr;

struct vm_args_s
{
    vm_ptr vm;
    char name[16];
    uint32_t ram_size;
}__attribute__((packed));
typedef struct vm_args_s vm_args;


struct iv2_s
{
    short x,y;
};
typedef struct iv2_s ivec2;

//
static SDL_Surface *active_surf_ptr = NULL;

static SDL_Surface *active_font_chars[16][256]; // colored

static uint32_t active_draw_color_fg=PD_COLOR_WHITE,
                active_draw_color_bg=PD_COLOR_BLACK;

//

#define PD_DEBUG

uint32_t get_register(vm_ptr vm, uint8_t _rd);
const char *ins_str(uint16_t ins);

void active_set_draw_color(uint32_t col)
{
    active_draw_color_fg = col;
}

void active_draw_putchar(vm_ptr vm, uint8_t ch, uint8_t col, ivec2 pos)
{
    if(vm->rvm == VIDMODE_TEXT_MONO)
    {
        active_draw_color_fg = PDCOLOR(PD_COLOR_LIGHT_GRAY);
        active_draw_color_bg = PDCOLOR(PD_COLOR_BLACK);
    }
    else if(vm->rvm == VIDMODE_TEXT_COLOR)
    {
        active_draw_color_fg = PDCOLOR(col&0xf);
        active_draw_color_bg = PDCOLOR((col>>4)&0xf);
    }
    else
    {
        printf("Incorrect text mode: %02x\n", vm->rvm);
        return;
    }

    SDL_Rect trect;
    trect.x = CHPOS_X(pos.x);
    trect.y = CHPOS_Y(pos.y);
    trect.w = CH_W;
    trect.h = CH_H;
    
    SDL_BlitSurface(active_font_chars[col][ch], NULL,
                    active_surf_ptr, &trect);
}

void active_draw_text_buff(vm_ptr vm, uint8_t *buff)
{
    ivec2 chpos = {0,0};
    for(int i=0; i < VIDMODE_TEXT_SIZE*2; i += 2)
    {
        if(buff[i] >= 0x20) // printable
        {
            active_draw_putchar(vm, buff[i], buff[i+1], chpos);
        }
        else
        {
            if(buff[i] == '\n')
                chpos.y++;
            else if(buff[i] == '\t')
                chpos.x += 4; // tab
            else if(buff[i] == '\r')
                chpos.x = 0;
            continue;
        }

        if(chpos.x++ >= 125)
        {
            chpos.x = 0;
            if(chpos.y++ >= 44)
                chpos.y = 0;
        }
        else
            chpos.x++;
    }
}

// Set @_rd to @v
void set_register(vm_ptr vm, uint32_t v, uint8_t _rd)
{
    if(_rd == R_R0L)
        vm->r0l = v&0xff;
    else if(_rd == R_R0H)
        vm->r0h = v&0xff;
    else if(_rd == R_R0X)
        vm->r0x = v&0xffff;
    else if(_rd == R_R0F)
        vm->r0f = v;
    else if(_rd == R_R0S)
        vm->r0s = v;

    else if(_rd == R_R1L)
        vm->r1l = v&0xff;
    else if(_rd == R_R1H)
        vm->r1h = v&0xff;
    else if(_rd == R_R1X)
        vm->r1x = v&0xffff;
    else if(_rd == R_R1F)
        vm->r1f = v;
    else if(_rd == R_R1S)
        vm->r1s = v;

    else if(_rd == R_RVX)
        vm->rvx = v&0xffff;
    else if(_rd == R_RVY)
        vm->rvy = v&0xffff;
    else if(_rd == R_RVC)
        vm->rvc = v&0xff;
    else if(_rd == R_RVM)
        vm->rvm = v&0xff;
    else if(_rd == R_RV0)
        vm->rv0 = v&0xff;
    else if(_rd == R_RV1)
        vm->rv1 = v&0xff;
    else if(_rd == R_RV2)
        vm->rv2 = v&0xff;
    else if(_rd == R_RV3)
        vm->rv3 = v&0xff;

    else if(_rd == R_RIP)
        vm->rip = v;
    else if(_rd == R_RSP)
        vm->rsp = v;
    else if(_rd == R_RBP)
        vm->rbp = v;
}

uint32_t get_register(vm_ptr vm, uint8_t _rd)
{
    uint32_t val=0;
    if(_rd == R_R0L)
        val = vm->r0l;
    else if(_rd == R_R0H)
        val = vm->r0h;
    else if(_rd == R_R0X)
        val = vm->r0x;
    else if(_rd == R_R0F)
        val = vm->r0f;
    else if(_rd == R_R0S)
        val = vm->r0s;

    else if(_rd == R_R1L)
        val = vm->r1l;
    else if(_rd == R_R1H)
        val = vm->r1h;
    else if(_rd == R_R1X)
        val = vm->r1x;
    else if(_rd == R_R1F)
        val = vm->r1f;
    else if(_rd == R_R1S)
        val = vm->r1s;

    else if(_rd == R_RVX)
        val = vm->rvx;
    else if(_rd == R_RVY)
        val = vm->rvy;
    else if(_rd == R_RVC)
        val = vm->rvc;
    else if(_rd == R_RVM)
        val = vm->rvm;
    else if(_rd == R_RV0)
        val = vm->rv0;
    else if(_rd == R_RV1)
        val = vm->rv1;
    else if(_rd == R_RV2)
        val = vm->rv2;
    else if(_rd == R_RV3)
        val = vm->rv3;

    else if(_rd == R_RIP)
        val = vm->rip;
    else if(_rd == R_RSP)
        val = vm->rsp;
    else if(_rd == R_RBP)
        val = vm->rbp;

    return val;
}

void inc_register(vm_ptr vm, uint8_t _rd)
{
    set_register(vm, get_register(vm, _rd)+1, _rd);
}

void dec_register(vm_ptr vm, uint8_t _rd)
{
    set_register(vm, get_register(vm, _rd)-1, _rd);
}

uint32_t get_sized_value(uint8_t sz, uint8_t* vstart)
{
    if(sz == 1)
        return (*vstart)&0xff;
    if(sz == 2)
        return (((uint16_t)vstart[0]<<8) | ((uint16_t)vstart[1]))&0xffff;
    if(sz == 4)
        return (  ((uint32_t)vstart[0]<<24)
                | ((uint32_t)vstart[1]<<16)
                | ((uint32_t)vstart[2]<<8)
                | ((uint32_t)vstart[3]))&0xffff;
    return 0;
}

uint32_t pop_stack(vm_ptr vm)
{
    if(vm->rsp < 4)
    {
        POP_ERR;
        return 0;
    }
    uint32_t v =  (((uint32_t)vm->ram_ptr[vm->rsp]) << 24)
                | (((uint32_t)vm->ram_ptr[vm->rsp+1]) << 16)
                | (((uint32_t)vm->ram_ptr[vm->rsp+2]) << 8)
                |   (uint32_t)vm->ram_ptr[vm->rsp+3];
    vm->rsp -= 4;
    return v;
}

void push_stack(vm_ptr vm, uint32_t val)
{
    vm->rsp += 4;

    vm->ram_ptr[vm->rsp + 0] = (val>>24)&0xff;
    vm->ram_ptr[vm->rsp + 1] = (val>>16)&0xff;
    vm->ram_ptr[vm->rsp + 2] = (val>>8)&0xff;
    vm->ram_ptr[vm->rsp + 3] =  val&0xff;
}

// Copy from @_rs to @_rd
void copy_register(vm_ptr vm, uint8_t _rs, uint8_t _rd)
{
    uint32_t val = 0;

    if(_rs == R_R0L)
        val = vm->r0l;
    else if(_rs == R_R0H)
        val = vm->r0h;
    else if(_rs == R_R0X)
        val = vm->r0x;
    else if(_rs == R_R0F)
        val = vm->r0f;
    else if(_rs == R_R0S)
        val = vm->r0s;

    else if(_rs == R_RIP)
        val = vm->rip;
    else if(_rs == R_RSP)
        val = vm->rsp;
    else if(_rs == R_RBP)
        val = vm->rbp;

    set_register(vm, val, _rd);
}

int exec_instruction(vm_ptr vm, uint16_t ins)
{
    uint16_t target = (ins&0xff00);
    uint8_t *ram_argstart = &vm->ram_ptr[vm->rbp+vm->rip + 2];
    if(target == VMB)
    {
        uint8_t sz = *ram_argstart; // size of arg a (arg b is always register)
        if(ins == R_VINT)
        {
            uint8_t int_id = ram_argstart[1];

            if(int_id == 0x76) // Video Init
            {
                if(vm->rvm == 0)
                    printf("Invalid vmode 0x00\n");
                else if(vm->rvm <= 2)
                {
                    vm->vid_buff_ptr = (uint8_t*)realloc(vm->vid_buff_ptr, VIDMODE_TEXT_SIZE*2);
                    memset(vm->vid_buff_ptr, 0, VIDMODE_TEXT_SIZE*2);
                }
                else if(vm->rvm == VIDMODE_LOWRES_256k)
                    vm->vid_buff_ptr = (uint8_t*)realloc(vm->vid_buff_ptr, VIDMODE_LOWRES_256K_SIZE);
                else
                    printf("Sorry, mode %02x is not supported.\n", vm->rvm);
            }
            else if(int_id == 0x80) // Print pixel/character/bitmap
            {
                if(vm->rvm <= 2)
                {
                    vm->vid_buff_ptr[ (vm->rvx + vm->rvy*80)*2 ]   = vm->rvc;
                    vm->vid_buff_ptr[ (vm->rvx + vm->rvy*80)*2+1 ] = vm->rv0;
                }
                else if(vm->rvm == VIDMODE_LOWRES_16k)
                    printf("Not currently supported, sorry\n");
                else if(vm->rvm == VIDMODE_LOWRES_256k)
                    vm->vid_buff_ptr[vm->rvx + vm->rvy*640] = vm->rvc;
            }

            printf(" $0x%02x", int_id);
            vm->rip += 2 + 2; // ins(2) + size(1) + arg(1)
        }
        else if(ins == R_VINC)
        {
            uint8_t reg_id = ram_argstart[1];
            inc_register(vm, reg_id);
            vm->rip += 2 + 1 + 1; // ins(2) + size(1) + reg(1)

            printf(" 0x%02x", reg_id);
        }
        else if(ins == R_VMOV)
        {
            uint8_t sp_shift = sz+1; // size(1) + var(size)

            if(sz == 0)
            {
                uint8_t _a = ram_argstart[1],
                        _b = ram_argstart[2];

                copy_register( vm, _a, _b ); // copy a to b
                sp_shift = 2; // register + sz

                printf(" 0x%02x, 0x%02x", _a, _b);
            }
            else if(sz > 4)
            {
                SIZE_ERR(sz);
            }
            else
            {
                uint32_t v = get_sized_value(sz, &ram_argstart[1]);
                uint8_t _b = ram_argstart[sz+1];
                set_register(vm, v, _b);

                printf(" $0x%02x, 0x%02x", v, _b);
            }
            vm->rip += 2 + sp_shift + 1; // ins(2) + sz(volatile) + b(1)
        }
        else if(ins == R_VMOVA)
        {
            if(ram_argstart[0] == 0) // reg
            {
                uint8_t _reg1 = ram_argstart[1]; // src addr
                uint8_t _reg2 = ram_argstart[2]; // dst reg

                set_register(vm, vm->ram_ptr[vm->rbp+get_register(vm, _reg1)], _reg2);

                vm->rip += 2 + 1 + 1 + 1; // ins + sz + reg1 + reg2
            }
            else if(ram_argstart[0] == 4)
            {
                uint32_t _addr = get_sized_value(4, &ram_argstart[1]);
                uint8_t _reg = ram_argstart[4+1];

                set_register(vm, vm->ram_ptr[vm->rbp+_addr], _reg);

                vm->rip += 2 + 1 + 4 + 1; // ins + sz + addr[4] + reg
            }
            else
            {
                SIZE_ERR(ram_argstart[0]);
            }
        }
    }
    else if(target == AMB) // audio
    {
        if(ins == R_AINT)
        {

        }
    }
    else if(target == IDB)
    {
        if(ins == R_JZ || ins == R_JNZ) // if zero, exec instruction next to current (otherwise skip 2 bytes more)
        {
            uint8_t cmpmod = (ins==R_JZ); // 1/0
            // ram_argstart[0] is sz, for this instruction it is always 0 -\_(^_^)_/-
            uint8_t _rtc = *ram_argstart; // register to compare
            uint32_t _regval = get_register(vm, _rtc);

            uint8_t passed = 0;
            if(_regval == 0 && cmpmod)
            {
                vm->rip += 2+1; // ins+reg (quit)
                passed = 1;
            }
            else
            {
                vm->rip += 2 + 1 + 2 + 1; // ins + reg + arg[ins] + arg[reg] (continue)
            }

            uint16_t _ins2 = ((uint16_t)ram_argstart[1]<<8) | (uint16_t)ram_argstart[2];
            uint8_t _arg2 = ram_argstart[3];
            printf(" 0x%02x -> %s 0x%02x (%s)",
                   _rtc,
                   ins_str(_ins2),
                   _arg2,
                   (passed == 1)?"TRUE":"FALSE");
        }
        else if(ins == R_JMP)
        {
            uint32_t ptr = get_sized_value(4, ram_argstart); // get all 4 bytes after instruction
            vm->rip = ptr;

            printf(" 0x%04x", ptr);
        }
    }
    else if(target == MMB)
    {
        uint8_t sz = *ram_argstart;
        if(ins == R_MOV)
        {
            uint8_t sp_shift = sz+1;

            if(sz == 0)
            {
                uint8_t _a = ram_argstart[1],
                        _b = ram_argstart[2];
                copy_register(vm, _a, _b);
                sp_shift = 2;

                printf(" 0x%02x, 0x%02x", _a, _b);
            }
            else if(sz > 4)
            {
                SIZE_ERR(sz);
            }
            else
            {
                uint32_t v = get_sized_value(sz, &ram_argstart[1]);
                uint8_t _b = ram_argstart[sz+1];
                set_register(vm, v, _b);

                printf(" $0x%02x, 0x%02x", v, _b);
            }
            vm->rip += 2 + sp_shift + 1;
        }
        else if(ins == R_MOVA)
        {
            uint8_t _from=0, _to=0;
            if(ram_argstart[0] == 0) // reg
            {
                uint8_t _reg1 = ram_argstart[1]; // src addr
                uint8_t _reg2 = ram_argstart[2]; // dst reg

                set_register(vm, vm->ram_ptr[vm->rbp+get_register(vm, _reg1)], _reg2);

                _from = _reg1;
                _to = _reg2;

                vm->rip += 2 + 1 + 1 + 1; // ins + sz + reg1 + reg2
            }
            else if(ram_argstart[0] == 4)
            {
                uint32_t _addr = get_sized_value(4, &ram_argstart[1]);
                uint8_t _reg = ram_argstart[4+1];

                set_register(vm, vm->ram_ptr[vm->rbp+_addr], _reg);

                vm->rip += 2 + 1 + 4 + 1; // ins + sz + addr[4] + reg

                _from = _addr;
                _to = _reg;
            }
            else
            {
                SIZE_ERR(ram_argstart[0]);
            }
            printf(" (0x%02x), 0x%02x", _from, _to);
        }
        else if(ins == R_INC)
        {
            uint8_t reg_id = *ram_argstart;
            inc_register(vm, reg_id);
            vm->rip += 2 + 1; // ins(2) + reg(1)

            printf(" 0x%02x", reg_id);
        }
        else if(ins == R_POP)
        {
            uint8_t r = *ram_argstart;
            uint32_t v = pop_stack(vm);
            set_register(vm, v, r);

            if(r != R_RIP)
                vm->rip += 2 + 1;

            printf(" 0x%02x", r);
        }
        else if(ins == R_PUSH)
        {
            uint32_t v = get_register(vm, *ram_argstart);

            push_stack(vm, v);

            vm->rip += 2 + 1;

            printf(" 0x%02x", *ram_argstart);
        }
    }
    else if(target == SMB)
    {
        if(ins == R_ADD)
        {
            uint8_t sz = *ram_argstart;
            if(sz == 0)
            {
                uint8_t _a = ram_argstart[1],
                        _b = ram_argstart[2];

                uint32_t v1 = get_register(vm, _a),
                         v2 = get_register(vm, _b);

                set_register(vm, v1+v2, _b);

                vm->rip += 2 + 1 + 1 + 1;

                printf(" $0x%02x, 0x%02x", _a, _b);
            }
            else if(sz == 1)
            {
                uint8_t _reg = ram_argstart[2];

                uint32_t v1 = ram_argstart[1];
                uint32_t v2 = get_register(vm, _reg);

                set_register(vm, v1+v2, _reg);

                vm->rip += 2 + 1 + 1 + 1;

                printf(" $0x%02x, 0x%02x", v1, _reg);
            }
            else // for now
            {
                SIZE_ERR(sz);
            }
        }
    }
    else
    {
        printf("ERR (0x%04x)\n", ins);
        vm->rip++; // do not stuck please (will stuck only if recognised instruction, but not data)
        return 1;
    }
    putchar('\n');
    return 0;
}

const char *ins_str(uint16_t ins)
{
    if(ins == R_HLT)
        return "hlt";
    if(ins == R_VINT)
        return "vint";
    if(ins == R_VMOV)
        return "vmov";
    if(ins == R_VMOVA)
        return "(vmov)"; // address from ram
    if(ins == R_VINT)
        return "vint";
    if(ins == R_VINC)
        return "vinc";
    if(ins == R_MOV)
        return "mov";
    if(ins == R_MOVA)
        return "(mov)";
    if(ins == R_ADD)
        return "add";
    if(ins == R_SUB)
        return "sub";
    if(ins == R_JZ)
        return "jz";
    if(ins == R_JNZ)
        return "jnz";
    if(ins == R_JMP)
        return "jmp";
    if(ins == R_INC)
        return "inc";
    if(ins == R_POP)
        return "pop";
    if(ins == R_PUSH)
        return "push";
    return "\0";
}

void *run_vm_p(void *vargs) // handles vm video and input
{
    if(vargs == NULL)
        return NULL;

    vm_args args = *(vm_args*)vargs;

    int quit=0;

    SDL_Event s_ev;
    while(!quit)
    {
        while(SDL_PollEvent(&s_ev))
        {
            if(s_ev.type == SDL_QUIT)
            {
                quit = 1;
                break;
            }
        }
        // Draw
        active_set_draw_color(args.vm->rv0);
        active_draw_text_buff(args.vm, args.vm->vid_buff_ptr);
        //
        SDL_Flip(active_surf_ptr);
    }

    pthread_exit(NULL);
}

void *audio_vm_p(void *vargs)
{
    if(vargs == NULL)
        return NULL;
    vm_args args = *(vm_args*)vargs;

    vm_ptr vm = args.vm;

    while(1)
    {

        if(vm->snd_buff_ptr == NULL)
            break;
    }

    pthread_exit(NULL);
}

void *exec_vm_p(void *vargs)
{
    if(vargs == NULL)
        return NULL;
    vm_args args = *(vm_args*)vargs;

#ifdef PD_DEBUG
    printf("VM %s started with %u bytes of RAM\n",
           args.name,
           args.ram_size);
#endif

    uint8_t *vm_ram_ptr = args.vm->ram_ptr, *cur_ram_ptr=NULL;

    while(1)
    {
        cur_ram_ptr = &vm_ram_ptr[args.vm->rbp + args.vm->rip];

        uint16_t cur_ins;
        cur_ins = ((uint16_t)cur_ram_ptr[0]<<8) | (uint16_t)cur_ram_ptr[1];
        if(cur_ins == R_HLT)
        {
            printf("%s halted.\n", args.name);
            break;
        }
        printf("[%04x:%04x] %s",
               args.vm->rbp,
               args.vm->rip,
               ins_str(cur_ins));

        int r = exec_instruction(args.vm, cur_ins);
        if(r)
            break;
    }

    pthread_exit(NULL);
}

void run_vm(vm_ptr vm, const char *vm_name)
{
    vm_args args;
    args.vm = vm;
    sprintf(args.name, "%s", vm_name);
    args.ram_size = 1024;
    //

    pthread_t drawt, audiot, exect;

    pthread_create(&drawt, NULL, run_vm_p, &args);
    pthread_create(&exect, NULL, exec_vm_p, &args);
    pthread_create(&audiot, NULL, audio_vm_p, &args);

    pthread_join(drawt, NULL);
}

vm_ptr create_vm(uint32_t vm_ramsize, uint32_t vm_stacksize)
{
    vm_ptr newvm = malloc(sizeof(struct vm_s));
    if(newvm == NULL)
        return NULL;

    newvm->r0f = 0;

    // Registers
    newvm->r0l = newvm->r0h = newvm->r0x = newvm->r0f = 0;
    newvm->r1l = newvm->r1h = newvm->r1x = newvm->r1f = 0;

    newvm->rip = 0;
    newvm->rbp = 0;
    newvm->rsp = 0;
    //

    // RAM
    newvm->ram_size = vm_ramsize;
    newvm->ram_ptr = (uint8_t*)malloc(newvm->ram_size);
    memset(newvm->ram_ptr, 0, newvm->ram_size);
    newvm->rbp = vm_stacksize; // stack size
    //

    // Video init
    newvm->rvm = VIDMODE_TEXT_MONO;
    newvm->rvx = newvm->rvy = newvm->rvc = 0;
    newvm->rv1 = newvm->rv2 = newvm->rv3 = 0;
    newvm->rv0 = 0x07;

    newvm->vid_buff_ptr = (uint8_t*)malloc( 80 * 25 );
    memset(newvm->vid_buff_ptr, 0, VIDMODE_TEXT_SIZE);
    //

    // Audio init
    newvm->rav = 127; // middle volume

    const size_t snd_buff_size = 5 * EMU_AUDIO_BPS;

    newvm->snd_buff_ptr = (uint8_t*)malloc(snd_buff_size);
    memset(newvm->snd_buff_ptr, 0, snd_buff_size);
    //

    return newvm;
}

void destroy_vm(vm_ptr vm)
{
    free(vm->snd_buff_ptr);
    free(vm->vid_buff_ptr);
    free(vm->ram_ptr);

    free( vm );
}

int init_font(const char *fntfile)
{
    TTF_Font *fnt = TTF_OpenFont(fntfile, 12);

    if(fnt == NULL)
        return 1;

    SDL_Color col;

    for(uint16_t i=0; i < 16; i++)
    {
        uint32_t col_u32 = PDCOLOR(i);
        col.r = (col_u32 >> 16) & 0xff;
        col.g = (col_u32 >> 8) & 0xff;
        col.b = (col_u32) & 0xff;
        col.unused = 0xff;

        for(uint16_t j=0; j < 256; j++)
        {
            active_font_chars[i][j] = TTF_RenderGlyph_Solid(fnt, j, col);
        }
    }

    TTF_CloseFont(fnt);
    return 0;
}

void deinit_font()
{
    for(uint16_t i=0; i < 16; i++)
        for(uint16_t j=0; j < 256; j++)
            SDL_FreeSurface(active_font_chars[i][j]);
    TTF_Quit();
}

int main()
{
    setlocale(LC_ALL, "ru_RU.KOI8-R");
    setlocale(LC_NUMERIC, "C");

    // SDL init
    SDL_Init(SDL_INIT_EVERYTHING);

    if(TTF_Init() < 0)
        return 2;

    // X init
    active_surf_ptr = SDL_SetVideoMode(EMU_WIDTH, EMU_HEIGHT,
                                       32,
                                       SDL_HWSURFACE | SDL_DOUBLEBUF);
    if(active_surf_ptr == NULL)
    {
        fprintf(stderr, "Failed to init SDL!\n");
        return -1;
    }

    init_font(EMU_FONT);

    // Default colors
    active_draw_color_bg = PD_COLOR_BLACK;
    active_draw_color_fg = PD_COLOR_LIGHT_GRAY;
    //

    const size_t ramsize=2048, stacksize = 256;
    const size_t codesize = ramsize - stacksize;

    vm_ptr myvm = create_vm(ramsize, stacksize);

    uint8_t *buff = (uint8_t*)malloc(codesize);

    FILE *myf = fopen("rom.bin", "rb");
    if(myf == NULL)
        printf("No ROM found. Place file rom.bin in emulator folder\n");
    fread(buff, 1, codesize, myf);
    fclose(myf);

    memcpy( (uint8_t*)(myvm->ram_ptr+stacksize), buff, codesize);

    free(buff);

    run_vm(myvm, "VM0");

    destroy_vm(myvm);

    SDL_FreeSurface(active_surf_ptr);
    SDL_Quit();

    return 0;
}
