TEMPLATE = app

TARGET=pdemu.x86_64

CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lSDL -lSDL_gfx -lSDL_mixer -lSDL_ttf -lpthread

INCLUDEPATH += ../ /usr/include/SDL/

SOURCES += \
        main.c
