#ifndef RMASM_H
#define RMASM_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

enum PD_BLOCK_MASKS
{
    MMB=0x0100,
    IDB=0x0200,
    SMB=0x0300,
    AMB=0x0500,
    VMB=0x0800,
};

enum PD_INSTRUCTIONS
{
    R_HLT=IDB|0xFF,
    R_JMP=IDB|0x04,

    R_JZ=IDB|0x0C,
    R_JNZ=IDB|0xF3,

    R_MOV=MMB|0x02,
    R_MOVA=MMB|0x20,
    R_INC=MMB|0x03,
    R_PUSH=MMB|0x08,
    R_POP=MMB|0x80,
    
    R_AMOV=AMB|0x02,
    R_AMOVA=AMB|0x20,
    R_AINT=AMB|0x07,
    R_AINC=AMB|0x03,

    R_VMOV=VMB|0x02,
    R_VMOVA=VMB|0x20,
    R_VINT=VMB|0x07,
    R_VINC=VMB|0x03,
    
    R_ADD=SMB|0x03,
    R_SUB=SMB|0xFC,
};

enum PD_VSIZES
{
    VSIZER  = 0x0, // register
    VSIZE8  = 0x1, // 1 byte value
    VSIZE16 = 0x2, // 2 bytes value
    VSIZE32 = 0x4, // 4 bytes value
};

enum PD_REGISTERS
{
    // General purpose
    R_R0L=0x02,
    R_R0H=0x04,
    R_R0X=0x06,
    R_R0F=0x08,
    R_R0S=0x10,
    
    R_R1L=0x01,
    R_R1H=0x03,
    R_R1X=0x05,
    R_R1F=0x07,
    R_R1S=0x09,
    //
    
    // IDB
    R_RIP=0x1C,
    R_RBP=0x20,
    R_RSP=0x24,
    //
    
    // VMB
    R_RVX=0x50,
    R_RVY=0x52,
    R_RVC=0x54,
    R_RVM=0x55,
    
    R_RV0=0x56,
    R_RV1=0x56,
    R_RV2=0x56,
    R_RV3=0x56,
    //
};

#define VIDMODE_TEXT_SIZE (uint16_t)(80*25)
#define VIDMODE_LOWRES_16K_SIZE (uint16_t)(640*480*0.5) // 2 colors per byte
#define VIDMODE_LOWRES_256K_SIZE (uint16_t)(640*480)

enum PD_VMODES
{
    VIDMODE_TEXT_MONO=0x1,
    VIDMODE_TEXT_COLOR=0x2,
    VIDMODE_LOWRES_16k=0x3,
    VIDMODE_LOWRES_256k=0x4,
};

//

enum PD_COLORS
{
    PD_COLOR_BLACK=0x0,
    PD_COLOR_BLUE=0x1,
    PD_COLOR_GREEN=0x2,
    PD_COLOR_CYAN=0x3,
    PD_COLOR_RED=0x4,
    PD_COLOR_PURPLE=0x5,
    PD_COLOR_ORANGE=0x6,
    PD_COLOR_LIGHT_GRAY=0x7,
    PD_COLOR_GRAY=0x8,
    PD_COLOR_LIGHT_BLUE=0x9,
    PD_COLOR_LIGHT_GREEN=0xa,
    PD_COLOR_LIGHT_CYAN=0xb,
    PD_COLOR_LIGHT_RED=0xc,
    PD_COLOR_LIGHT_PURPLE=0xd,
    PD_COLOR_YELLOW=0xe,
    PD_COLOR_WHITE=0xf,
};

static const uint32_t pd_color_palette_16k[16] =
{
    0x000000,
    0x0000aa,
    0x00aa00,
    0x00aaaa,
    0xaa0000,
    0xaa00aa,
    0xaa5500,
    0xaaaaaa,
    0x555555,
    0x5555ff,
    0x55ff55,
    0x55ffff,
    0xff5555,
    0xff55ff,
    0xffff55,
    0xffffff
};

#define PDCOLOR(id) pd_color_palette_16k[id]

#ifdef __cplusplus
};
#endif

#endif
